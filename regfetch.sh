#!/bin/bash

function w3m_download() {
    w3m -dump "$1" > "${2}.html"
}
function wget_download() {
    wget "$1" -O "${2}"
}

function newscat_download() {
    newscat "$1" > "${2}.html"
}

# Check if the specified tool is installed
## $1 is the tool to check for
function check_tool() {
    if [[ "$1" = "w3m" ]] ; then
        if command -v w3m>/dev/null; then
            METHOD="w3m_download"
        else
            echo -e "\e[0;31mw3m is not in PATH\e[m"
            exit 1
        fi
    elif [[ "$1" = "wget" ]] ; then
        if command -v wget>/dev/null; then
            METHOD="wget_download"
        else
            echo -e "\e[0;31mwget is not in PATH\e[m"
            exit 1
        fi
    elif [[ "$1" = "newscat" ]] ; then
        if command -v newscat>/dev/null; then
            METHOD="newscat_download"
        else
            echo -e "\e[0;31mnewscat is not in PATH\e[m"
            exit 1
        fi
    else
        echo -e "\e[0;31mInvalid method!\e[m"
        echo "Available methods: w3m, wget, netcat"
        exit 1
    fi    
}

# Check if the provided arguments are correct
function check_args() {
    if [ "$#" -lt 1 ]; then
        echo 'Usage: ./regfetch.sh METHOD "URL" REGEX FOLDER [options]'
        echo 'Example: ./regfetch.sh wget "https://drewdevault.com" "2020" drew' 
        echo ''
        exit 1
    elif [ "$#" -eq 1 ]; then
        echo -e "\e[0;31mURL not specified\e[m"
        exit 1
    elif [ "$#" -eq 2 ]; then
        echo -e "\e[0;31mRegex not specified\e[m"
        exit 1
    elif [ "$#" -eq 3 ]; then
        echo -e "\e[0;31mFolder not specified\e[m"
        exit 1
    elif [ "$#" -gt 3 ]; then
        echo -e "\e[0;33m[Additional options not yet supported]\e[m"
    fi
}

# Gets the urls from the page
## $1 is the directory to put the urls in
## $2 is the page to search
## $3 is the regex
function fetch_urls() {
    echo -e "\e[0;34mMaking directory...\e[m"
    mkdir -p "$1">/dev/null || echo "\e[0;34mWriting to existing directory...\e[m"
    cd "$1" || exit 1

    echo -e "\e[0;34mFetching urls...\e[m"
    lynx -dump -listonly "$2" | grep -E "$3" | awk '{print $2}' | uniq -u > urls
    URL_COUNT="$(wc -l urls | awk '{print $1}')"

    if [ "$URL_COUNT" -eq 0 ]; then
        echo -e "\e[0;31mNo urls found!\e[m"
        exit 1
    else
        echo -e "\e[0;32mFound $URL_COUNT urls!\e[m"
    fi

    read -r -p "Manually select urls? (y/n): " -n 1 INPUT
    echo
    if [[ $INPUT =~ ^[Yy]$ ]]; then
        SELECTED_URLS=$(fzf --prompt='select urls> '-m -i -e --preview='w3m -dump {}' < urls)
        echo "$SELECTED_URLS" > urls
        SELECTED_URLS_COUNT=$(echo "$SELECTED_URLS" | wc -l)
        echo -e "\e[0;34m Selected $SELECTED_URLS_COUNT urls!\e[m"
    fi
}

# Download the pages defined in the urls file
function download_pages() {
    read -r -p "Proceed? (y/n): " -n 1 INPUT
    echo
    if [[ ! $INPUT =~ ^[Yy]$ ]]
    then
        echo -e "\e[0;31mAborting...\e[m"; exit 1
    fi

    echo -e "\e[0;34mExtracting html pages...\e[m"
    while read -r LINE; do
        if [[ "$LINE" =~ '/'$ ]]; then
            ARTICLE=$(echo "$LINE" | awk -F/ '{print $(NF-1)}')
        else
            ARTICLE=$(echo "$LINE" | awk -F/ '{print $(NF)}')
        fi
        echo -e "\e[0;34mDownloading article \"$ARTICLE\"\e[m"
        eval $METHOD "$LINE" "$ARTICLE"
        echo -e "\e[0;32mArticle \"$ARTICLE\" downloaded!\e[m"
    done < urls
}

######################################################################
# START
######################################################################
check_args "$@"
check_tool "$1"
fetch_urls "$4" "$2" "$3"
download_pages

