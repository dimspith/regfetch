# Regfetch (WIP!)

## Overview
Regfetch is a tool to download all urls on a page that match a regex. 
It can be used to download news, articles or every link in a page depending on your needs.
The script doesn't yet support recursing into links but i will hopefully add it.

## Dependencies
* `lynx`
* `fzf`
* `awk`
* `w3m`
* Either `wget` or `newscat`

## Installation
Just clone the repository and run the script

## Usage
./regfetch.sh \<method\> \<url\> \<regex\> \<folder\>

